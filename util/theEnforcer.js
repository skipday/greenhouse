const MongooseAdapter = require('@elastic.io/casbin-mongoose-adapter')
const { newEnforcer } = require("casbin")
module.exports = {
    get:async()=>{
        const dbUri=process.env.MONGODB
        const dbOptions= { useNewUrlParser: true,useCreateIndex: true }
        const adapter = await MongooseAdapter.newAdapter(dbUri,dbOptions)
        const enforcer = await newEnforcer("authz_model.conf", adapter)
        return enforcer
    }
}