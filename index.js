require('dotenv').config()

const express = require("express")
const authz = require("casbin-express-authz")
const mongoose = require('mongoose')

const dbUri=process.env.MONGODB
const dbOptions= { useNewUrlParser: true,useCreateIndex: true }
const bodyParser=require("body-parser")
const helmet=require("helmet")
const app = express()
const fs = require('fs')
const User=require('./model/user')
const theEnforcer=require('./util/theEnforcer')
const {OAuth2Client} = require('google-auth-library')
const client = new OAuth2Client(process.env.GOOGLE_CLIENT_ID)
mongoose.connect(dbUri,dbOptions, err=> {
    if (err)console.log("Mongo Error=>", err)
})
app.set('view engine', 'ejs')
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))
app.use(helmet())
app.use(async(req, res, next) => {
    let firstPath=req.path.split('/')[1]
    let isGuest=firstPath=='guest'||firstPath==''
    req.locals = req.locals || {}
    let username=isGuest?'guest':''
    if(!isGuest&&req.query.token){
        try{
            let ticket=await client.verifyIdToken({
                idToken: req.query.token,
                audience: process.env.GOOGLE_CLIENT_ID
            })
            const payload = ticket.getPayload()
            const userid = payload['sub']
            let user=await User.findOne({
                token:req.query.token
            })
            console.log(payload)
            if(user)username=user._id
        }catch(e){
            console.log(e.message)
        }
    }
    req.locals.currentUser = {username}
    req.locals.authenticated = !!username
    next()
})

app.use(
    authz(async () => {
        let enforcer=await theEnforcer.get()
        return enforcer
    })
)
app.get('/', function (req, res) {
    res.render('guest')
})
app.route('/:module/:method*').all(async(req, res) => { 
    console.log("access")
    let reqModule=req.params.module
    let reqMethod=req.params.method
    let file='./controller/'+(reqModule+'.js'||'-')
    if (!fs.existsSync(file)) {
        res.status(404).send('module not found')
        return
    }
    let selectedModule=require(file)
    if(!selectedModule[reqMethod]){
        res.status(404).send('method not found')
        return
    }
    try{
        await selectedModule[reqMethod](req,res)
    }catch(e){
        res.status(500).json({
            success:false,
            message: e.message
        })
    }
})
app.listen(process.env.PORT, async() =>{
    console.log(`app listening on port ${process.env.PORT}!`)
    let enforcer=await theEnforcer.get()

    /** 
     * one time call only
    */
    await createGuestRole(enforcer)
    await createGodRole(enforcer)
    await createVendorRole(enforcer)
    await createCustomerRole(enforcer)
    await createAdminRole(enforcer)
})


createGuestRole=async(enforcer)=>{
    //can access home page
    await enforcer.addPolicy('guest', '/', '*')

    //can access every page with 'guest' at the beginning
    await enforcer.addPolicy('guest', '/guest*', '*')
}

createGodRole=async(enforcer)=>{
    //can access all functions
    await enforcer.addPolicy('god', '/*', '*')
}

createCustomerRole=async(enforcer)=>{
    //can access all costumer functions
    await enforcer.addPolicy('customer', '/customer*', '*')
    
    //can manage profile
    await enforcer.addPolicy('customer', '/user/profile*', '*')
}

createVendorRole=async(enforcer)=>{
    //can access all vendor functions
    await enforcer.addPolicy('vendor', '/vendor*', '*')

    //can manage profile
    await enforcer.addPolicy('vendor', '/user/profile*', '*')
}

createAdminRole=async(enforcer)=>{
    //can access all admin functions
    await enforcer.addPolicy('admin', '/admin*', '*')

    //can manage vendor
    await enforcer.addGroupingPolicy('admin', 'vendor')

    //can manage customer
    await enforcer.addGroupingPolicy('admin', 'customer')
}