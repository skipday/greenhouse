const {OAuth2Client} = require('google-auth-library');
const client = new OAuth2Client(process.env.GOOGLE_CLIENT_ID);
const User=require('../model/user')
const CasbinRule=require('../model/casbin_rule')
const theEnforcer=require('../util/theEnforcer')
const speakeasy=require('speakeasy')
module.exports = {
    view:async(req,res)=>{
        res.render('guest')
    },
    twoFaAuthView:async(req,res)=>{
        res.render('twoFaAuthView')
    },
    login:async (req,res)=>{
        console.log("accesssss")
        let user=await User.findOne({ email:req.body.email })
        if(!user){
            user=await new User({
                email:req.body.email,
                name:req.body.name,
                token:req.body.token,
            }).save()
            let enforcer=await theEnforcer.get()
            let iamGod=await enforcer.addGroupingPolicy(user._id, 'god')
            res.status(200).json({
                success:true,
                data: {
                    view:'god/view'
                }
            })
        }else {
            if(user.twoFaAuth&&user.twoFaAuth.length>0&&user.token&&user.token.length==0){
                user.token=""
                await user.save()
                res.status(200).json({
                    success:true,
                    data: {
                        view:'guest/twoFaAuthView'
                    }
                })
                return
            }
            user.token=req.body.token
            user.lastLogin=Date.now()
            await user.save()
            let view=await CasbinRule.findOne({ v0:user._id })
            if(view){
                console.log("render")
                console.log(view.v1)
                res.status(200).json({
                    success:true,
                    data: {
                        view:view.v1+'/view'
                    }
                })
            }
            else {
                res.status(200).json({
                    success:true,
                    data: "no view"
                })
            }
        }
    },
    validate2Fa:async (req,res)=>{
        let user=await User.findOne({ email:req.body.email })
        console.log(user)
        if(!user)throw ({message:"user not found"})
        var verified = speakeasy.totp.verify({
            secret: user.twoFaAuth,
            encoding: 'base32',
            token: req.body.twoFaAuthToken
        })
        if(!verified){
            res.status(403).json({
                success:false,
                data: "code invalid or expired"
            })
            return
        }
        let view=await CasbinRule.findOne({ v0:user._id })
        if(view){
            console.log("render")
            console.log(view.v1)
            user.token=req.query.token
            user.lastLogin=Date.now()
            await user.save()
            res.status(200).json({
                success:true,
                data: {
                    view:view.v1+'/view'
                }
            })
        }
        else {
            res.status(200).json({
                success:true,
                data: "no view"
            })
        }
    },
    logout:async (req,res)=>{
        let user=await User.findOne({ email:req.query.token })
        user.token=""
        let a=await user.save()
        res.status(200).json({
            success:true,
            data: "no view"
        })
    }
}
