const User=require('../model/user')
module.exports = {
    view:async(req,res)=>{
        res.render('vendor')
    },
    commLeads:async (req,res)=>{
        res.status(200).json({
            success:true,
            data: "commLeads"
        })
    },
    commentReview:async (req,res)=>{
        res.status(200).json({
            success:true,
            data: "commentReview"
        })
    },
    uploadDocuments:async (req,res)=>{
        res.status(200).json({
            success:true,
            data: "uploadDocuments"
        })
    }
}