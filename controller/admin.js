const User=require('../model/user')
module.exports = {
    view:async(req,res)=>{
        res.render('admin')
    },
    manageReview:async (req,res)=>{
        res.status(200).json({
            success:true,
            data: "manageReview"
        })
    },
    viewReports:async (req,res)=>{
        res.status(200).json({
            success:true,
            data: "viewReports"
        })
    },
    viewConnComm:async (req,res)=>{
        res.status(200).json({
            success:true,
            data: "viewConnComm"
        })
    },
}