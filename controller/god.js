const theEnforcer=require('../util/theEnforcer')
const User=require('../model/user')
removeGod=async(id)=>{
    let enforcer=await theEnforcer.get()
    let notGodAnymore = await enforcer.removeGroupingPolicy(id, 'god')
    return enforcer
}
getUser=async(req)=>{
    let user=await User.findOne({
        token:req.query.token
    })
    return user
}

module.exports = {
    view:async(req,res)=>{
        res.render('god')
    },
    downgradeToAdmin:async(req,res)=>{
        let user=await getUser(req)
        let enforcer=await removeGod(user._id)
        let justAdmin=await enforcer.addGroupingPolicy(user._id, 'admin')
        res.status(200).json({
            success:true,
            data: "just admin"
        })
    },
    downgradeToCustomer:async(req,res)=>{
        let user=await getUser(req)
        let enforcer=await removeGod(user._id)
        let justCustomer=await enforcer.addGroupingPolicy(user._id, 'customer')
        res.status(200).json({
            success:true,
            data: "just customer"
        })
    },
    downgradeToVendor:async(req,res)=>{
        let user=await getUser(req)
        let enforcer=await removeGod(user._id)
        let justVendor=await enforcer.addGroupingPolicy(user._id, 'vendor')
        res.status(200).json({
            success:true,
            data: "just vendor"
        })
    },
}