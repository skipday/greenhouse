const User=require('../model/user')
module.exports = {
    view:async(req,res)=>{
        res.render('customer')
    },
    searchVendor:async (req,res)=>{
        res.status(200).json({
            success:true,
            data: "searchVendor"
        })
    },
    viewVendor:async (req,res)=>{
        res.status(200).json({
            success:true,
            data: "viewVendor"
        })
    },
    viewVendorReviews:async (req,res)=>{
        res.status(200).json({
            success:true,
            data: "viewConnComm"
        })
    },
    connVendor:async (req,res)=>{
        res.status(200).json({
            success:true,
            data: "connVendor"
        })
    },
    reviewVendor:async (req,res)=>{
        res.status(200).json({
            success:true,
            data: "reviewVendor"
        })
    },
}