const User=require('../model/user')
const speakeasy = require('speakeasy');
module.exports = {
    view:async(req,res)=>{
        res.render('user')
    },
    manageProfile:async (req,res)=>{
        res.status(200).json({
            success:true,
            data: "user"
        })
    },
    toggleF2a:async (req,res)=>{
        let secret = speakeasy.generateSecret({length: 20});
        console.log(secret.base32);
        let user=await User.findOne({ token:req.query.token })
        if(!user) throw "no user"
        user.twoFaAuth=secret.base32
        await user.save()
        res.status(200).json({
            success:true,
            data: secret.base32
        })
    },
}