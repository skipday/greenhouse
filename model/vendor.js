const mongoose     = require('mongoose');
const Schema       = mongoose.Schema;

const Vendor   = new Schema({
    user: {type : Schema.Types.ObjectId, ref : 'User'},
    category:String,
    numberOfEmployee:Number,
    dateCreated: {type : Number, default : Date.now},
    dateModified: {type : Number, default : Date.now},
});

module.exports = mongoose.model('Vendor', Vendor,'vendors');