const mongoose     = require('mongoose');
const Schema       = mongoose.Schema;

const User   = new Schema({
    token:String,
    email:String,
    name:String,
    phone:String,
    photo:String,
    twoFaAuth:String,
    dateCreated: {type : Number, default : Date.now},
    dateModified: {type : Number, default : Date.now},
    lastLogin: {type : Number, default : Date.now},
});

module.exports = mongoose.model('User', User,'users');