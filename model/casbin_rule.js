const mongoose     = require('mongoose');
const Schema       = mongoose.Schema;

const CasbinRuleSchema   = new Schema({
    p_type:String,
    v0:String,
    v1:String,
    v2:String,
});

module.exports = mongoose.model('CasbinRule', CasbinRuleSchema,'casbin_rule');