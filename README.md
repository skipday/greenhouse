
This is not a complete project.

**DONE**

 1. Google Sign In (API)
 2. 2FA (API)
 3. Role management (API)

**TODO:**

 1. Program Flow
 2. Add option to activate 2FA on user profile page
 3. After registration redirect to 'God' view so user can choose what role they want (available: admin, vendor, customer) and then redirect it to appropriate view (admin, vendor, or customer)
 4. Add View for each role
 5. Add API Search Vendor, View Vendor, View Vendor Reviews, Connect to Vendor, and Review Vendor to Customer
 6. Add API Manage Profile to All Role
 7. Add API Communicate With Leads, Comment on Review, and Upload Document to Vendor
 8. Add API Manage Review, View Reports, and View Connections & Communicate to Admin
  

## Preparation
Run mongodb with authentication set to disabled on 127.0.0.1 and port 27017

```bash

yarn install

```

## Run on local server

```bash

node index.js

```

open `localhost:3000`